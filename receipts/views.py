from django.shortcuts import render, redirect
from .models import Receipt, Account, ExpenseCategory
from .forms import CreateReceiptForm, CreateCategoryForm, CreateAccountForm
from django.contrib.auth.decorators import login_required


@login_required
def receipt_list(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)

    context = {
        "receipt_list": receipt_list
    }

    return render(request, "receipts/list.html", context)


@login_required
def account_list(request):
    account_list = Account.objects.filter(owner=request.user)

    context = {
        "account_list": account_list
    }

    return render(request, "accounts/list.html", context)


@login_required
def category_list(request):
    category_list = ExpenseCategory.objects.filter(owner=request.user)

    context = {
        "category_list": category_list
    }

    return render(request, "categories/list.html", context)


@login_required
def create_reciept(request):
    if request.method == 'POST':
        form = CreateReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('home')
    else:
        form = CreateReceiptForm()

    context = {
        'form': form,
    }

    return render(request, 'receipts/create.html', context)


@login_required
def create_category(request):
    if request.method == 'POST':
        form = CreateCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect('category_list')
    else:
        form = CreateCategoryForm()

    context = {
        'form': form,
    }

    return render(request, 'categories/create.html', context)


@login_required
def create_account(request):
    if request.method == 'POST':
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect('account_list')
    else:
        form = CreateAccountForm()

    context = {
        'form': form,
    }

    return render(request, 'accounts/create.html', context)
